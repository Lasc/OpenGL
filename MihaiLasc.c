/*
Mihai Lasc - Student Number 13108336

Declaration on Plagiarism

I declare  that  this  material,  which  I now  submit for  assessment, is  entirely  my own work and has not been taken from the work of others, save and to the extent that such work  has  been  cited  and  acknowledged  within  the  text  of  my work.  

Program usage

- Shapes are available by right click on the white canvas.
- Brightness and transparency settings are available by right click on the Colour Picker window. 

Particularities:
- the circles (both hollow and full) are drawn by two left button clicks - one for the centre and the other for the radius.
- the polygon can have as many vertices as the user wishes. It will be closed when the user clicks the right mouse button.
- the rectangles (both hollow and full) are drawn by two left clicks of the mouse, one for each of the two opposites corners. 
- colour selection is done by left click on the colour picker window. The selected colour is applied to all subsequent shapes.
- brightness is increased and decreased by subsequent selections from the right click menu. If the last shape drawn on the canvas is a full circle or full rectangle, the brightness adjustment will apply to it. All other subsequent shapes are drawn with the adjusted brightness colour, unless another colour is selected from the colour picker. 
- transparency can be enabled and disabled and it applies to the shapes that are drawn after the user enables or disables it. Transparency can also be varied by the user with a step of 0.1 by selection of one of the two SubMenu options.
*/


#include <GL/glut.h> 
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define PIXEL_SIZE 2

int canvasWindow, colorPickerWindow;
int menuOption;

enum
{
	MENU_POINTS=0,
	MENU_LINES,
	MENU_CIRCLES,
	MENU_POLYGON,
	MENU_RECTANGLES,
	MENU_HOLLOW_CIRCLE,
	MENU_HOLLOW_RECTANGLE,
	MENU_COLOR_PICKER,
};
int buttonPressed = 0;
int firstPoint ;
GLfloat firstX, firstY;
int pixelsOnX, pixelsOnY;
int pixelSize = PIXEL_SIZE;
int sceneH = 650;
int sceneW = 1000;

// last drawnFigurine
int lastMenuOp;

//coordinates of last drawn rectangle
int lsStartX, lsStartY, lsEndX, lsEndY;

//coordinates of last drawn circle
int lsCenterX, lsCenterY, lsRadius;

//current color on the canvas
float color[4] = {1.0, 0.0, 0.0,1.0};

int k=1;
int X[10],Y[10];

float r,g,b,brightness=1.0;
unsigned char selected_color[3];


void userChosenEntry(int value) {
	switch (value) {
	case 1:
		menuOption = MENU_POINTS;
		break;
	case 2:
		menuOption = MENU_LINES;
		firstPoint = 0;
		break;
	case 3:
		menuOption = MENU_CIRCLES;
		firstPoint = 0;
		break;
	case 4:
		menuOption = MENU_POLYGON;
		break;		
	case 5:
		menuOption = MENU_RECTANGLES;
		firstPoint = 0;
		break;
	case 6:
		menuOption = MENU_HOLLOW_CIRCLE;
		firstPoint = 0;
		break;
	case 7:
		menuOption = MENU_HOLLOW_RECTANGLE;
		firstPoint = 0;
		break;
	case 9:
		glClear(GL_COLOR_BUFFER_BIT);
		glFinish();
		firstPoint = 0;
	
		break;
	case 10:
		exit(1);
		break;
	}

}

void point(int x, int y, float color[4]) {
	glColor4f(color[0], color[1], color[2], color[3]);
	glBegin( GL_POINTS);
	glVertex2i(x * pixelSize + pixelSize / 2, y * pixelSize + pixelSize / 2);
	glEnd();
	glFlush();
	
}

void menuEntry(int value) {
	userChosenEntry(value);
}

void createMenu(void) {
	glutCreateMenu(menuEntry);
	glutAddMenuEntry("Point", 1);
	glutAddMenuEntry("Line", 2);
	glutAddMenuEntry("Filled Circle", 3);
	glutAddMenuEntry("Hollow Polygon", 4);

	glutAddMenuEntry("Filled Rectangle", 5);
	glutAddMenuEntry("Hollow Circle", 6);
	glutAddMenuEntry("Hollow Rectangle", 7);
	
	
	glutAddMenuEntry("Clear Screen", 9);
	glutAddMenuEntry("Exit", 10);
	glutAttachMenu ( GLUT_RIGHT_BUTTON);
	
}
void redraw(void) {
	glClear( GL_COLOR_BUFFER_BIT);
	glFinish();
	firstPoint = 0;
}

void drawLine(int firstx, int firsty, int secondx, int secondy) {
	int dx = abs(secondx - firstx), sx = firstx < secondx ? 1 : -1;
	int dy = -abs(secondy - firsty), sy = firsty < secondy ? 1 : -1;
	int err = dx + dy, e2; 

	for (;;) 
	{
		point(firstx, firsty, color);
		if (firstx == secondx && firsty == secondy)
			break;
		e2 = 2 * err;
		if (e2 >= dy) {
			err += dy;
			firstx += sx;
		}
		if (e2 <= dx) {
			err += dx;
			firsty += sy;
		}
	}
	glFlush();
}

void drawFilledCircle(int x, int y, int radius) {
	glColor4f(color[0], color[1], color[2], color[3]);

	glBegin(GL_TRIANGLE_FAN);
	glVertex2f (x,y);
	for (int i = 0; i <=360; i++)
	{				
		glVertex2f(x+radius*cos(i*M_PI/180), y+radius*sin(i*M_PI/180));
	}
	glEnd();
}
void drawHollowCircle(int x0, int y0, int radius) {
	glColor4f(color[0], color[1], color[2], color[3]);

	int f = 1 - radius;
	int ddF_x = 1;
	int ddF_y = -2 * radius;
	int x = 0;
	int y = radius;

	point(x0, y0 + radius, color);
	point(x0, y0 - radius, color);
	point(x0 + radius, y0, color);
	point(x0 - radius, y0, color);

	while (x < y) {
		if (f >= 0) {
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;
		point(x0 + x, y0 + y, color);
		point(x0 - x, y0 + y, color);
		point(x0 + x, y0 - y, color);
		point(x0 - x, y0 - y, color);
		point(x0 + y, y0 + x, color);
		point(x0 - y, y0 + x, color);
		point(x0 + y, y0 - x, color);
		point(x0 - y, y0 - x, color);
	}
	glFlush();

}

void drawRectangle(int firstx, int firsty, int secondx, int secondy) {
	int length = abs(secondx - firstx);
	int width = abs(secondy - firsty);
	drawLine(firstx, firsty, firstx + length, firsty);
	drawLine(firstx + length, firsty, firstx + length, firsty - width);
	drawLine(firstx + length, firsty - width, firstx, firsty - width);
	drawLine(firstx, firsty - width, firstx, firsty);
	glFlush();
}

void filledRectangle(int firstx, int firsty, int secondx, int secondy)
{
	glColor4f(color[0], color[1], color[2], color[3]);
	glRectf(firstx, firsty, secondx, secondy);
}

int rCoordinates(double coordinate)
{
	 return (coordinate>=0) ? (int)(coordinate+0.5):(int)(coordinate-0.5);
}
void setPixelPolygon(GLint x,GLint y)
{
 glBegin(GL_POINTS);
 glVertex2f(x,y);
 glEnd();
}
void line(int x0,int y0,int x1,int y1)
{
 int lineY=y1-y0;
 int lineX=x1-x0;
 int iterations,i;
 float increaseX,increaseY,x=x0,y=y0;
 if(abs(lineX)>abs(lineY))
 {
  iterations=abs(lineX);
 }
 else
 {
  iterations=abs(lineY);
 }
 increaseX=(float)lineX/(float)iterations;
 increaseY=(float)lineY/(float)iterations;
 setPixelPolygon(rCoordinates(x),rCoordinates(y));
 for(i=0;i<iterations;i++)
 {
  x+=increaseX;
  y+=increaseY;
  setPixelPolygon(rCoordinates(x),rCoordinates(y));
 }
 glutSwapBuffers();
}

void draw_polygon(int button, int state, int x, int y)
{

  switch(button)
 {
    case GLUT_LEFT_BUTTON:
     if (state == GLUT_DOWN)
     {   

      X[k]=x;
      Y[k]=sceneH-y; 

      if(k%2==0)
      {
       line(X[k-1],Y[k-1],X[k],Y[k]);
      }
      else if(k>1 && k%2!=0)
      {
       line(X[k-1],Y[k-1],X[k],Y[k]);
      }

      glutSwapBuffers();
      k++; 
     }

     break;
   default:
     break;
   }
    fflush(stdout); 
}

void mouse(int button, int state, GLfloat x, GLfloat y) {
	switch (menuOption) {
	case MENU_POINTS:
		k=1;
		lastMenuOp = menuOption;
		point(x, y, color);
		break;

	case MENU_LINES:
		k=1;
		lastMenuOp = menuOption;
		if (!firstPoint) {
			firstX = x;
			firstY = y;
			firstPoint = 1;
		}
		else {
			drawLine(firstX, firstY, x, y);
			firstPoint = 0;
		}
		break;

	case MENU_CIRCLES:
		k=1;
		lastMenuOp = menuOption;
		if (!firstPoint) {
			firstX = x;
			firstY = y;
			firstPoint = 1;
		} else {
		
			int radius = (int) sqrt(
					(double) ((x - firstX) * (x - firstX) + (y - firstY)
							* (y - firstY)));

			drawFilledCircle(x, y, radius);
			
			lastMenuOp = 2;
			lsCenterX = x;
			lsCenterY =y;
			lsRadius = radius;
			
			firstPoint = 0;
		}

		break;
	case MENU_POLYGON:
		lastMenuOp = menuOption;
		draw_polygon( button,state, x, y);
		break;
		
	case MENU_RECTANGLES:
		k=1;
		lastMenuOp = menuOption;
		if (!firstPoint) {
			firstX = x;
			firstY = y;
			firstPoint = 1;
		}

		else {
	       
			filledRectangle(firstX, firstY, x, y);
			lsStartX = firstX;
			lsStartY = firstY;
			lsEndX = x;
			lsEndY = y;
			
			
			firstPoint = 0;
		}
		break;

	case MENU_HOLLOW_CIRCLE:
		k=1;
		lastMenuOp = menuOption;
		if (!firstPoint) {
			firstX = x;
			firstY = y;
			firstPoint = 1;
		} else {
		
			int radius = (int) sqrt(
					(double) ((x - firstX) * (x - firstX) + (y - firstY)
							* (y - firstY)));

			drawHollowCircle(x, y, radius);

			firstPoint = 0;
		}

		break;

	case MENU_HOLLOW_RECTANGLE:
		k=1;
		lastMenuOp = menuOption;
		if (!firstPoint) {
			firstX = x;
			firstY = y;
			firstPoint = 1;
		} else {
		
			int radius = (int) sqrt(
					(double) ((x - firstX) * (x - firstX) + (y - firstY)
							* (y - firstY)));

			drawRectangle(firstX, firstY, x, y);
			firstPoint = 0;
		}

		break;		
	case MENU_COLOR_PICKER:
		k=1;
		lastMenuOp = menuOption;
		break;
	};
}
void mouseClick(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
			
		if ((menuOption == 2) || (menuOption == 4))
		{
			mouse(button, state, x ,(sceneH - 1) - y );
		}
		else if (menuOption == 3)
		{
			mouse(button, state, x ,y);
		}else
		{
			mouse(button, state, x / pixelSize,
				((sceneH - 1) - y) / pixelSize);
		}
		buttonPressed = 1;
	} else if (button == GLUT_LEFT_BUTTON) {
		buttonPressed = 0;
	}

}

void init() {
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glColor3f(color[0], color[1], color[2]);
	
	glMatrixMode( GL_PROJECTION);
	glLoadIdentity();
	
	gluOrtho2D(0.0, (GLdouble) sceneW, 0.0, (GLdouble) sceneH);
	
	glPointSize((GLfloat) pixelSize);
	glEnable( GL_POINT_SMOOTH);
	glEnable( GL_LINES);
	glEnable( GL_TRIANGLE_FAN);
	glEnable( GL_POLYGON_SMOOTH);
	glEnable( GL_LINE_SMOOTH);

	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

}

void colorPicker()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBegin(GL_QUADS);
        glColor3f(0.5f, 0.0f, 1.0f); // purple
        glVertex2f(-0.75, 0.75);
        glColor3f(1.0f, 0.0f, 0.0f); // red
        glVertex2f(-0.75, -0.75);
        glColor3f(0.0f, 1.0f, 0.0f); // green
        glVertex2f(0.75, -0.75);
        glColor3f(1.0f, 1.0f, 0.0f); //yellow
        glVertex2f(0.75, 0.75);
    glEnd();
	glutSwapBuffers();
	return ;
}

void mouseColorPicker(int button , int state, int x,int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
		if (glutGetWindow()==1)
		{
			glutDisplayFunc(redraw);
		}
		else
		{
			glutDisplayFunc(colorPicker) ;
			glReadPixels(x ,glutGet( GLUT_WINDOW_HEIGHT ) - y , 1 , 1 , GL_RGBA , GL_UNSIGNED_BYTE , selected_color);
			r = selected_color[0]/255.0;
			color[0] = r;
			g = selected_color[1]/255.0;
			color[1] = g;
			b = selected_color[2]/255.0;
			color[2] = b;
		}

	return;
}

void TranspSubMenu(int id)
{
	if ( id == 2) 
	{
		glutSetWindow ( canvasWindow ) ;

		glEnable( GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		color[3] += 0.1;
	}
	if ( id == 1) 
	{
		glutSetWindow ( canvasWindow ) ;

		glEnable( GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		color[3] -= 0.1;
	}
	
}
	void colorPickerMenu ( int id )
	{
		if ( id == 1) exit ( 0 ) ;
		if ( id == 2) 
		{
			color[0] +=0.1;
			color[1] +=0.1;
			color[2] +=0.1;
			// if last drawn figurine is a filled circle 
			// increase its brightness
			if (lastMenuOp == 2)
			{
				glutSetWindow ( canvasWindow ) ;
				drawFilledCircle(lsCenterX, lsCenterY, lsRadius);
			}
			// if last drawn figurine is a filled rectangle 
			// increase its brightness
			if (lastMenuOp == 4)
			{
				glutSetWindow ( canvasWindow ) ;
				filledRectangle(lsStartX, lsStartY, lsEndX, lsEndY);
			}			
		}
		if ( id == 3) 
		{
			color[0] -=0.1;
			color[1] -=0.1;
			color[2] -=0.1;
			
			// if last drawn figurine is a filled circle 
			// decrease its brightness
			if (lastMenuOp == 2)
			{
				glutSetWindow ( canvasWindow ) ;
				drawFilledCircle(lsCenterX, lsCenterY, lsRadius);
			}
			// if last drawn figurine is a filled rectangle 
			// decrease its brightness
			if (lastMenuOp == 4)
			{
				glutSetWindow ( canvasWindow ) ;
				filledRectangle(lsStartX, lsStartY, lsEndX, lsEndY);
			}				
		}
		
		if ( id == 5) 
		{
			glutSetWindow ( canvasWindow ) ;

			glDisable( GL_BLEND);
			color[3] =1;
		}

		return;
	}
	
	void myReshape(int w, int h )
{
	glViewport(0,0,w,h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	colorPicker();
	return ;
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(sceneW, sceneH);
	glutInitWindowPosition(50, 50);
	canvasWindow = glutCreateWindow("Mihai Lasc - 2D Drawing Tool");
	glutDisplayFunc(redraw);

	glutMouseFunc(mouseClick);
	createMenu();
	init();
	
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(200, 200);

	glutInitWindowPosition(1080 , 50);
	colorPickerWindow = glutCreateWindow(" Color Picker ");
	glutDisplayFunc(colorPicker);
	glutReshapeFunc(myReshape);

	glutMouseFunc(mouseColorPicker);
		
	int transparencySubMenu = glutCreateMenu(TranspSubMenu);
	glutAddMenuEntry("Increase Transparency", 1);
	glutAddMenuEntry("Decrease Transparency", 2);
	
	glutCreateMenu(colorPickerMenu);
	glutAddMenuEntry("quit",1);
	glutAddMenuEntry("increase brightness",2);
	glutAddMenuEntry("decrease brightness",3);
	glutAddSubMenu("enable transparency",transparencySubMenu);
	glutAddMenuEntry("disable transparency",5);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
	

	glutMainLoop();
	return 0;
}