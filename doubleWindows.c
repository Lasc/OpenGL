

#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>
#include <stdio.h>

#define DEG_TO_RAD 0.017453

int singleb , doubleb ;
GLfloat theta = 0.0 ;
int globalX, globalY;
float r,g,b,brightness=1.0;

 unsigned char color[3];

void displays()
{

	glClear(GL_COLOR_BUFFER_BIT);
		glEnable( GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBegin(GL_POLYGON);
	glColor4f(r, g, b, brightness);
	//glColor4f(r, g, b, 0.1f); 
	glVertex2f(cos(DEG_TO_RAD*theta),sin(DEG_TO_RAD*theta));
	glVertex2f(-sin(DEG_TO_RAD*theta),cos(DEG_TO_RAD*theta));
	glVertex2f(-cos(DEG_TO_RAD*theta),-sin(DEG_TO_RAD*theta));
	glVertex2f(sin(DEG_TO_RAD*theta),-cos(DEG_TO_RAD*theta));
    //glColor3f(color[0], color[1], color[2]);
	printf("%f %f %f %f \n", r, g, b, brightness);
	glEnd();
	glutSwapBuffers();
	
	//glFlush();
}

void displayd()
{
	glClear(GL_COLOR_BUFFER_BIT);
	
/*	
	glBegin(GL_POLYGON);
	glVertex2f(cos(DEG_TO_RAD*theta),sin(DEG_TO_RAD*theta));
	glVertex2f(-sin(DEG_TO_RAD*theta),cos(DEG_TO_RAD*theta));
	glVertex2f(-cos(DEG_TO_RAD*theta),-sin(DEG_TO_RAD*theta));
	glVertex2f(sin(DEG_TO_RAD*theta),-cos(DEG_TO_RAD*theta));
	glEnd();
*/	
	       glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
       glBegin(GL_QUADS);
               glColor3f(0.5f, 0.0f, 1.0f); // make this vertex purple
               glVertex2f(-0.75, 0.75);
               glColor3f(1.0f, 0.0f, 0.0f); // make this vertex red
               glVertex2f(-0.75, -0.75);
               glColor3f(0.0f, 1.0f, 0.0f); // make this vertex green
               glVertex2f(0.75, -0.75);
               glColor3f(1.0f, 1.0f, 0.0f); // make this vertex yellow
               glVertex2f(0.75, 0.75);
       glEnd();
	   
/*	   
	glLineWidth(10);
    glBegin(GL_LINES);
    glColor3f(1.0f, 1.0f, 0.0f); // make this vertex yellow
               //minus
			   glVertex2f(-0.25f, 1.0f); // vertex 1
               glVertex2f(-0.5f, 1.0f); // vertex 2

			   //plus
               glVertex2f(-0.75f, 1.0f); // vertex 1
               glVertex2f(-1.0f, 1.0f); // vertex 2
			   
			   
    glEnd();
*/	
	
	glutSwapBuffers();
	
	return ;
}

void display_stall()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POLYGON);

	glVertex2f(1.0,0.0);
	glVertex2f(0.0,1.0);
	glVertex2f(-1.0,0.0);
	glVertex2f(0.0,-1.0);
		glEnd();
	glFlush();
}
void spinDisplay(void)
{
	/*
	theta+=2.0;
	if ( theta > 360.0)
		theta-=360.0 ;
	*/

//	printf("%d %d \n", globalX,globalY);

	// draw s i n g l e b u f f e r e d window
	glutSetWindow ( singleb ) ;
	
	glutPostWindowRedisplay( singleb ) ;
	
/*	
	// draw doubl e b u f f e r e d window
	glutSetWindow ( doubleb ) ;
	glutPostWindowRedisplay ( doubleb ) ;
*/
	return ;
}
void mouse(int button , int state, int x,int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
		if (glutGetWindow()==1)
		{
			//printf("%d %d \n", x,y);
			glutDisplayFunc(displays);
		}
		else
		{
//			printf("%d %d \n", x,y);
			glutDisplayFunc(displayd) ;
			globalX = x;
			globalY = y;
			glReadPixels(x , y , 1 , 1 , GL_RGB , GL_UNSIGNED_BYTE , color);
			r = color[0]/255.0;
//			printf("red= %f \n", r);
			g = color[1]/255.0;
//			printf("green= %f \n", g);
			b = color[2]/255.0;
//			printf("blue= %f \n", b);
		}
	if ( button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN)
		glutDisplayFunc (display_stall) ;
	return;
}

void myReshape(int w, int h )
{
	glViewport(0,0,w,h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-2.0,2.0,-2.0,2.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	return ;
}
	void mykey(int key)
	{
		if(key=='Q'||key=='q') exit(0) ;
		return;
	}
	void quitmenu ( int id )
	{
		if ( id == 1) exit ( 0 ) ;
		if ( id == 2) 
			brightness+=0.1;
		if ( id == 3) 
			brightness-=0.1;
		return;
	}

	
int main (int argc , char ** argv )
{
	
	glutInit (&argc , argv );
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	singleb = glutCreateWindow(" s i n g l e b u f f e r e d ");
	glutDisplayFunc(displays);
	glutReshapeFunc(myReshape);
	glutIdleFunc(spinDisplay);
	glutMouseFunc(mouse);
	glutKeyboardFunc(mykey);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowPosition(400 , 0);
	doubleb = glutCreateWindow(" doubl e b u f f e r e d ");
	glutDisplayFunc(displayd);
	glutReshapeFunc(myReshape);
	glutIdleFunc(spinDisplay);
	glutMouseFunc(mouse);
	glutCreateMenu(quitmenu);
	glutAddMenuEntry("quit",1);
	glutAddMenuEntry("increase brightness",2);
	glutAddMenuEntry("decrease brightness",3);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
	glutMainLoop();
}

